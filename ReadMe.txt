-------------------------------- SCORPIOCODING.NET --------------------------------
2016-08-28
Author 	: Kribo
	: http://scorpiocoding.net
	: ScSimMvc 
	: Simple Mvc for small projects

Version : 2.0	:	2016-08-30

----------------- THE CORE DIR STRUCTURE -----------------

.htaccess							-	handles the user friendly urls
index.php							- 	Default router

html/								- 	public folder
	index.php						-	the Request

app/
	.htaccess						-	Options -Indexes
	init.php						-	the Instantiator

	core/
		App.php						- 	the Wrapper			
		Controller.php				-	the core Controller class
		Model.php					-	the core Model class
		View.php					-	the core View class
        Functions.php               -   the core Functions class  
	
	libs/


	controllers/					-	the handlers
		cFrontend.php
		cBackend.php
		cError.php

	models/							-	the data & business logic


	views/							-   the  renders & templates
		backend/
			dashboard.phtml
			templates/
				header.phtml
				footer.phtml

		frontend/
			home.phtml
			contact.phtml
			templates/
				header.phtml
				footer.phtml







----------------- USER FRIENDLY URLS -----------------
.htaccess
User not friendly url 		:	"" http://example.com/html/?page=Simple-Mvce&n=200 "" 
User friendly url	        :	"" http://example.com/html/Simple-Mvc/200 "" 


----------------- THE DEFAULT ROUTER -----------------

index.php
  	This Index.php is the default router to /html/index.php
	Instead of "Location /public_html/html/Index/index"
  	We just write "Location /public_html/Index/index"
  	Why ?
  	Because the .htaccess file and the "RewriteBase /public_html/html"
   	Why the .htaccess file ?
  	Because we want to separate concerns public and non public access
  	Visitor are only allowed in the html folder and not in the app folder.


----------------- THE REQUEST -----------------

calls an instance of the init.php



----------------- THE INSTANTIATOR -----------------

requires 	App.php
inits 		app = new App();

----------------- THE WRAPPER -----------------

WRAPS the controller / Methon (action) / PARAMS / VIEW all together.
ROUTER 
	handles the user friendly url 
	the routing of controller / method/ view
	
DISPATCHER
	handles the rendering



----------------- THE DEVELOPMENT FASES -----------------

FASE 1 --------------------------------------
	File structure created
	Routing Controller and Views up and running
		url - token [0] platform f || b => frontend or backend => Controller
		url - token [1] Method / view
		renderOptions 1,2,3
			1	-	rendering only of the view no templates
			2	-	rendering of the frontend with templates
			3	-	rendering of the backend with templates

FASE 2 --------------------------------------
	File structure : removed the view derectories per method :> just 2 Dir's
	ROUTING View.php (base View) and templating up and running

FASE 3 --------------------------------------
	MOD : libs/functions.php  --> CLASS core/Functions.php
	MOD : Functions.php :: Global errorhandeling funnction
	MOD : App.php		--> extends Functions
	MOD : View.php 		--> extends Functions
	MOD : Controller 	--> extends Functions
	MOD : Model.php 	--> extends Functions

FASE 4 --------------------------------------
	ROUTING - Security of subfolders of app/
	MOD : added in every subfolder an .htaccess file
	NOTE : .htaccess files only work if your server allows it.
	WHY ? : To stop direct access to app and its subfolders
	!!! : There is most probaliy a better way.
	!!! : the files are all iether php or phtml and server only readable
	!!! : not a 100% secure thing and needs improvement and additional testing

FASE 5 --------------------------------------
	Routing PARAMS
		url - token [2] => blog postId 	     / Login	userId
		url - token [3] => blog postTitle    / Login    Auth

	MOD : App.php --> using the unset( $token )
	    : so that the rebasing of the token array into params works correctly
		: call_user_func_array([self::$controller, self::$method], self::$params );
		: calls the controller and the method within that controller and passes the params into that method 
        : eg.    home -> index (params[0],params[2],params[3],....... etc)

FASE 6 --------------------------------------
	TESTING OF ROUTING OF PARAMS
	- created a frontend view blogPost.php which excepts 2 params id and title
	- how thats done  params[0][ 0....n] / params[1][0....n] / params[2][0....n] -> method( $id = array(), $title = array()
	- put the array is a single array $data = array( 'id' => $id, 'title' => $title );
	- then pass $data to the view whch in turn extracts it and inserts into the template.
	- ---------------------- IMPORTANT ------------
	-when creating the ASSOCIATIVE ARRAY $data the keys that you assing must be the same variable names in your template.
	-so in your template eg header.php and the <title><?= $title ?></title> 
	- yes <?= ?> are php short tags which must be turned on upon your server "" short_open_tag=On "" in your PHP.INI file
	- use the phpInfo() to check your server.

--------------------------------------RELEASE 1.0 -----  VERSION 2.0------------------------------------------------------------------------------


IMPORTANT --------------------------------------
	DETAILS TO REMEMBER : WHEN CHANGING THE ROOT FILENAME REMEBER TO MODIFY THE FOLLOWING FILES 
	MOD 	:	Default router : index.php  and mod the url
	MOD     :   .htaccess  file and mod the RewriteBase


FASE 7 --------------------------------------
	DATABASE INCLUSION
	CREATE 	: core/Database.php  that extends PDO
	CREATE 	: core/Model.php the root model class. with a dB in the constructor.


FASE 8 --------------------------------------
	STYLING INCLUSION
	CREATE 	: 	folder css
	CREATE 	: 	app.css
	CREATE 	: 	./scorpiocoding/default.css	
	MOD 	:	View.php renderOptions
	MOD		:	cFrontend.php the renderOptions
	MOD		:	cBackend.php the renderOptions
	MOD		:	renderOptions -> keys from 1,2,3   to  'f0' , 'f1' and 'b0', 'b1'
			:	making it posible to create more render options accordingly
	MOD 	: 	f/templates/header.php  
				-- adding the php code between the html-comment tags makes the links unclickable
				-- making the life of a code ripper alittle bit more ...... lol
	MOD 	:	core/Controller.php
				-- function setStyles ($name)  requiring the name of the css file
				-- function getStyles()
				-- protected property $_scStyles  = array()
	MOD 	:	config.php   - added global variable PATH_ABS  --> http://example.com/scsimmvc-2.0/html/
	MOB		:   cFrontend.php  - METOD : blogpost 


FASE 9 --------------------------------------
	To handle routing of sessions we first have to create a login system of sorts so...
	Login :

FASE 10 --------------------------------------
	Routing Sessions