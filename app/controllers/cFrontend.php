<?php

/**
 *  cFrontend
 */
class Frontend extends Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function home()
	{
		$this->view->render('f1', 'home');
	}

	public function about()
	{
		$this->view->render('f0', 'about');
	}

	public function contact()
	{
		$this->view->render('f1', 'contact');
	}

	public function project()
	{
			$this->view->render('f1', 'pjroject');
	}

	public function blogPost($id = array(), $title = array() )
	{
		$scVariables = self::getScVariables();
		$scVariables['scTitle'] = __FUNCTION__;

		//input
		$input = array( 'id' => $id, 'title' => $title );

		self::setStyles('app');
		self::setStyles('sc');
		$scStyles = self::getStyles();

		$data = array_merge($scVariables, $scStyles, $input);

		$this->view->render('f1', 'blogPost' , $data);
	}
}
