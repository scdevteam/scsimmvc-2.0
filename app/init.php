<?php
// ------------------------------ THE INSTANTIATOR -------------------------------------------

// --- !! DEFINE PATH_APP TO THE CURRENT DIRECTORY !!--
define("DS" , DIRECTORY_SEPARATOR );
define("PATH_APP" , __DIR__ . DS);  
define("PATH_CORE" , PATH_APP . "core" . DS);
// --- !! DEFINE PATH_APP TO THE CURRENT DIRECTORY !!--

require PATH_CORE . "Functions.php";
require_once PATH_CORE . "App.php";
$app = new App();


/*
*   We have to define PATH_APP here in the INIT.php because
*   the init.php file is in the derectory app.
*   Why?
*   Becuase of the RewriteBase rule in the .htaccess file
*   which obscures the absolute path for the directory app
*
*/

?>

